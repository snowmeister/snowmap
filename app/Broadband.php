<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Broadband extends Model
{
    //
    protected $table = "broadband";
    public $timestamps = false;
}

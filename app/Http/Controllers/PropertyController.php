<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function doAdzunaSearch($wherename=null,$numbedrooms=null, $minprice=null, $maxprice=null, $count=null, $page=null, $distance=null, $radius=null){
      $adzKey =  env('ADZUNA_API_KEY');
      $adzAppId =  env('ADZUNA_APPID');
      $url="http://api.adzuna.com:80/v1/api/property/gb/search/$page?app_id=$adzAppId&app_key=$adzKey&results_per_page=$page&where=$wherename&distance=$distance&category=for-sale&beds=$numbedrooms&price_min=$minprice&price_max=$maxprice";
      $adzunadata = $this->get_data($url);
      return $adzunadata;
    }
    public function getZoooplaSalesSearch($lat, $lng, $radius, $minprice, $maxprice,$numbedrooms, $pagenum, $pagesize)
    {
      $zooplakey=env('ZOOPLAKEY');
      $minprice = $minprice * 1000;
      $maxprice = $maxprice * 1000;
      $url = "http://api.zoopla.co.uk/api/v1/property_listings.json?latitude=$lat&longitude=$lng&minimum_beds=$numbedrooms&maximum_beds=$numbedrooms&minimum_price=$minprice&maximum_price=$maxprice&radius=$radius&page_number=$pagenum&listing_status=sale&page_size=$pagesize&api_key=$zooplakey";
      $zoopladata = $this->get_data($url);

      return $zoopladata;
    }
    public function getZoooplaLettingSearch($lat, $lng, $radius, $minprice, $maxprice,$numbedrooms, $pagenum, $pagesize)
    {
      
      $zooplakey=env('ZOOPLAKEY');
      $minprice = $minprice;
      $maxprice = $maxprice;
      $url = "http://api.zoopla.co.uk/api/v1/property_listings.json?latitude=$lat&longitude=$lng&minimum_beds=$numbedrooms&maximum_beds=$numbedrooms&minimum_price=$minprice&maximum_price=$maxprice&radius=$radius&page_number=$pagenum&listing_status=rent&page_size=$pagesize&api_key=$zooplakey";
      $zoopladata = $this->get_data($url);
      return $zoopladata;
    }
    public function searchByTownAndPostcode($town, $postcode, $page){

     return $this->getZoooplaSalesSearh("$town, $postcode", 10, $page);
      // http://api.adzuna.com:80/v1/api/property/gb/search/1?app_id=2ac205fc&app_key=8a7f7a55a0a0da80b0634d1464482c01&results_per_page=10&where=PO15%206NX&category=for-sale
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

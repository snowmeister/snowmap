<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CourtsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function getCourtsByPostcode($postcode) {
            // $url = 'https://courttribunalfinder.service.gov.uk/search/results.json?postcode=' . $postcode;
            
            $url = 'https://www.find-court-tribunal.service.gov.uk/search/results.json?postcode='.$postcode;
            $courtsData = json_decode($this->get_data($url));

            $arrCourts = [];
            foreach ($courtsData as $court) {
              # code...
              $courtdata = [
                "data"=>$court,
                "point_type"=>"court",
                "point"=>[$court->lat,$court->lon]
              ];
              $arrCourts[]=["item"=>$courtdata];
            }
            $arrData = ["results"=>$arrCourts, "total"=>count($arrCourts)];
           return $arrData ;
        }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pricepaid extends Model
{
    //

    protected $table = "pricepaid";
    public $timestamps = false;
}

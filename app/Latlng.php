<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Latlng extends Model
{
    //
    protected $table = "Postcode";
    public $timestamps = false;
}

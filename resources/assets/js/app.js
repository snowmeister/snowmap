/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 HTMLElement.prototype.serialize = function(){
     var obj = {};
     var elements = this.querySelectorAll( "input, select, textarea" );
     for( var i = 0; i < elements.length; ++i ) {
         var element = elements[i];
         var name = element.name;
         var value = element.value;

         if( name ) {
             obj[ name ] = value;
         }
     }
     return JSON.stringify( obj );
 }
 // TODO - This should be in a utils lib!!
 String.prototype.toProperCase = function(opt_lowerCaseTheRest) {
   return (opt_lowerCaseTheRest ? this.toLowerCase() : this)
     .replace(/(^|[\s\xA0])[^\s\xA0]/g, function(s) {
       return s.toUpperCase();
     });
 };

require('./bootstrap');

// TODO - Are these REALLY the ONLY way to do it? Hate being DIRTY with GLOBAL!
window.Vue = require('vue');
window.moment = require('moment');
window._ = require('lodash');


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example', require('./components/Example.vue'));
Vue.component('startbox', require('./components/Startbox.vue'));
Vue.component('tools', require('./components/Tools.vue'));
Vue.component('nolocation', require('./components/Nolocation.vue'));
Vue.component('locationdisplay', require('./components/Locationdisplay.vue'));
Vue.component('broadband', require('./components/Broadband.vue'));
Vue.component('schools', require('./components/Schools.vue'));
Vue.component('crime', require('./components/Crimes.vue'));
Vue.component('courts', require('./components/Courts.vue'));
Vue.component('property', require('./components/Property.vue'));
Vue.component('bedroomspicker', require('./components/Bedroomspicker.vue'));
Vue.component('radiuspicker', require('./components/Radiuspicker.vue'));
Vue.component('propertyitem', require('./components/Propertyitem.vue'));
Vue.component('propertysearch', require('./components/Propertysearchform.vue'));
Vue.component('zooplafooter', require('./components/Zooplafooter.vue'));
Vue.component('loadingspinner', require('./components/Loadingspinner.vue'));
Vue.component('nopropertyresults', require('./components/Nopropertyresults.vue'));
Vue.component('propertysearchsummary', require('./components/Propertysearchsummary.vue'));
Vue.component('pagination', require('./components/Pagination.vue'));
Vue.component('maptype', require('./components/Maptype.vue'));
Vue.component('cars', require('./components/Cars.vue'));
Vue.component('carsearch', require('./components/Carsearch.vue'));
Vue.component('carmaker', require('./components/Carmaker.vue'));
Vue.component('carcolours', require('./components/Carcolours.vue'));
Vue.component('caritem', require('./components/Caritem.vue'));
Vue.component('carsearchsummary', require('./components/Carsearchsummary.vue'));
Vue.component('carnoresults', require('./components/Carnoresults.vue'));
Vue.component('snowfooter', require('./components/Snowfooter.vue'))
Vue.component('about', require('./components/About.vue'))
Vue.component('alert', require('./components/Alert.vue'))
Vue.component('broadbanderror', require('./components/Broadbanderror.vue'))


const app = new Vue({
  el: '#app',
  data: {
    position: null,
    currentsalespaginationstart: 1,
    currentpagintationend: 20,
    toolsopen: true,
    showpropertysalessearch: true,
    showpropertylettingssearch: true,
    propertyloaded: false,
    currentWindow: null,
    showbroadband: false,
    wronglocation: false,
    currentmaptype: 'roadmap',
    numbedrooms: 3,
    carsearch: {},
    mincarprice: 1000,
    maxcarprice: 3000,
    currentpropertysearchtype: 'Sales',
    minrent: 100,
    explore: false,
    maxrent: 2000,
    carradius: 10,
    lettings: [],
    minsalesprice: 200,
    maxsalesprice: 300,
    propertyradius: 5,
    properties: [],
    loadingsearchdata: false,
    currentpropertypage: 1,
    searchresultscount: 20,
    cartotalresults: 0,
    cars: [],
    carsloaded: false,
    currentcarpage: 1,
    propertypagination: {
      sales: {
        currentstart: 1,
        currentend: 20,
        currentpage: 1,
        paginationcount: 0
      },
      lettings: {
        currentstart: 1,
        currentend: 20,
        currentpage: 1,
        paginationcount: 0
      },
      cars: {
        currentstart: 1,
        currentend: 20,
        currentpage: 1,
        paginationcount: 0
      }
    },
    currenterror: null,
    paginationcountSales: 0,
    paginationcountLettings: 0,
    paginationcount: 0,
    lettingspaginationcount: 0,
    markers: [],
    courttypes: [
      "Adoption",
      "Bankruptcy",
      "Children",
      "Civil partnership",
      "Crime",
      "Divorce",
      "Domestic violence",
      "Employment",
      "Forced marriage and FGM",
      "High Court District Registry",
      "Housing possession",
      "Immigration",
      "Money claims",
      "Probate",
      "Social security"
    ],
    courts: [],
    schools: [],
    crimes: [],
    colors: ['#f59c1e',
          '#cadfaa', //'#ff9119',
          '#9bc0fd', //'#ffb666',
          '#b9d2b9', //'#ffc27f',
          '#ffe199', //'#e4e4e4',
          '#9abffd', //'#ff9d33',
          '#ffffff', //'#ff8500',
          '#f0ede5', //'#c9c9c9',
          '#76a33f', //'#ffa94c',
          '#ffdab2',
          '#ffe7cc',
          '#ffce99',
          '#848484',
          '#929292',
          '#a0a0a0',
          '#adadad',
          '#bbbbbb',
          '#d6d6d6',
          '#484848',
          '#5d5d5d',
          '#717171',
          '#858585',
          '#999999',
          '#aeaeae'
      ],
    waiting: false,
    currentZoom: 6,
    minZoom: 6,
    postcodeerror: false,
    maxZoom: 19,
    postcodes: null,
    nolocation: false,
    arrCrimeChartData: [],
    arrCrimeChartOutcomeData: [],
    broadband: null,
    defaultpoint: [54.297944, -2.570313],
    pieoptions: {
      fontSize: 18,
      pieHole: 0.6,
      chartArea: {
        top: 20,
        width: '110%',
        height: '80%',
        backgroundColor: '#777',
      },
      pieSliceText: 'none',
      pieSliceBorderColor: '#777',
      pieSliceTextStyle: {
        color: 'white'
      },
      colors: ['#f59c1e',
            '#cadfaa', //'#ff9119',
            '#9bc0fd', //'#ffb666',
            '#b9d2b9', //'#ffc27f',
            '#ffe199', //'#e4e4e4',
            '#9abffd', //'#ff9d33',
            '#ffffff', //'#ff8500',
            '#f0ede5', //'#c9c9c9',
            '#76a33f', //'#ffa94c',
            '#ffdab2',
            '#ffe7cc',
            '#ffce99',
            '#848484',
            '#929292',
            '#a0a0a0',
            '#adadad',
            '#bbbbbb',
            '#d6d6d6',
            '#484848',
            '#5d5d5d',
            '#717171',
            '#858585',
            '#999999',
            '#aeaeae'
        ],
      backgroundColor: 'transparent',
      legend: {
        position: 'labeled',
        textStyle: {
          color: '#fff',
          fontSize: 12
        }
      },
      tooltip: {
        trigger: 'selection',
        textStyle: {
          color: '#777',
          fontSize: 13,
          bold: false,
          italic: false
        }
      }
    },
  },
  methods: {
    resetPagination: function(){
      app.propertypagination.sales.currentstart = 1;
      app.propertypagination.sales.currentend = 20;
      app.propertypagination.sales.currentpage = 1;
      app.propertypagination.sales.paginationcount = 0;

      app.propertypagination.lettings.currentstart = 1;
      app.propertypagination.lettings.currentend = 20;
      app.propertypagination.lettings.currentpage = 1;
      app.propertypagination.lettings.paginationcount = 0;

      app.propertypagination.cars.currentstart = 1;
      app.propertypagination.cars.currentend = 20;
      app.propertypagination.cars.currentpage = 1;
      app.propertypagination.cars.paginationcount = 0;
    },
    kilometersToMiles: function(kilometers) {
      var miles;
      miles = (kilometers * 0.62137);
      miles = Math.round(miles * 100) / 100;
      return miles;
    },
    formatPrice: function(price) {
      return '£' + price.toString().replace(/\B(?=(\d{3})+(?!\d))/, ',')
    },
    milesToKilometers: function(miles) {
      var km;
      km = (miles / 0.62137);
      km = Math.round(km * 100) / 100;
      return km;
    },
    deg2rad: function(deg) {
        return deg * (Math.PI / 180);
    },
    getDistanceFromLatLonInMiles: function(lat1, lon1, lat2, lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = app.deg2rad(lat2 - lat1); // deg2rad below
        var dLon = app.deg2rad(lon2 - lon1);
        var a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(app.deg2rad(lat1)) * Math.cos(app.deg2rad(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = app.kilometersToMiles(R * c); // Distance in km
        return d.toFixed(2);
    },
    checkCrimeArray: function(strCrime) {
      var crime = strCrime.trim();
      var intExists = 0;
      for (var p = 0; p < app.arrCrimeChartData.length; p++) {
        if (app.arrCrimeChartData[p].category.trim() === crime) {
          intExists++;
        }
      }
      if (intExists === 0) {
        return false;
      } else {
        return true;
      }
    },
    calculateNewRecordEnd: function() {
      var newend;
      newend = app.propertypagination[app.currentpropertysearchtype.toLowerCase()].currentpage * app.searchresultscount
      if(app.propertypagination[app.currentpropertysearchtype.toLowerCase()].currentpage < app.propertypagination[app.currentpropertysearchtype.toLowerCase()].paginationcount){
        app.propertypagination[app.currentpropertysearchtype.toLowerCase()].currentend = newend;
      }else{
        if(app.currentpropertysearchtype === 'Sales'){
            app.propertypagination[app.currentpropertysearchtype.toLowerCase()].currentend =  app.properties.result_count
        }else   if(app.currentpropertysearchtype === 'Lettings'){
          app.propertypagination[app.currentpropertysearchtype.toLowerCase()].currentend =  app.lettings.result_count
        }else{
          app.propertypagination[app.currentpropertysearchtype.toLowerCase()].currentend =  app.cartotalresults;

        }
      }
    },
    calculateNewRecordStart: function() {
      var newstart;
      newstart = ((app.propertypagination[app.currentpropertysearchtype.toLowerCase()].currentpage * app.searchresultscount) - app.searchresultscount) + 1;
      app.propertypagination[app.currentpropertysearchtype.toLowerCase()].currentstart = newstart;
    },
    checkOutcomeArray: function(strCrime) {
      var crime = strCrime.trim();
      var intExists = 0;
      for (var p = 0; p < app.arrCrimeChartOutcomeData.length; p++) {
        if (app.arrCrimeChartOutcomeData[p].outcome !== undefined) {
          if (app.arrCrimeChartOutcomeData[p].outcome.trim() === crime) {
            intExists++;
          }
        }
      }
      if (intExists === 0) {
        return false;
      } else {
        return true;
      }
    },
    resetCarSearchPanel: function(){
      console.log('reset the car search');
      app.cars = [];
      app.cartotalresults = 0;
      app.carsloaded = false
      app.loadingsearchdata = false;
    },
    incrementCrimeChartData: function(crime) {
      for (var i = 0; i < app.arrCrimeChartData.length; i++) {
        if (app.arrCrimeChartData[i].category === crime) {
          app.arrCrimeChartData[i].count++;
        }
      }
    },
    incrementOutcomeData: function(crime) {
      for (var i = 0; i < app.arrCrimeChartOutcomeData.length; i++) {
        if (app.arrCrimeChartOutcomeData[i].outcome === crime) {
          app.arrCrimeChartOutcomeData[i].count++;
        }
      }
    },
    processCrimeDataforCharts: function() {
      app.arrCrimeChartData = [];
      app.arrCrimeChartOutcomeData = []
      $.each(app.crimes, function(index, crime) {
        //bounds.extend(marker.position);
        var strCrime = crime.item.data.category;
        var boolCrimeExists = app.checkCrimeArray(strCrime);
        var strOutcome = 'Not Available';
        if (crime.item.data.outcome_status != null) {
          strOutcome = crime.item.data.outcome_status.category;
        }
        var outcomeExistsInChart = app.checkOutcomeArray(strOutcome);
        if (outcomeExistsInChart === false) {
          app.arrCrimeChartOutcomeData.push({
            'outcome': strOutcome,
            'count': 1
          });
        } else {
          app.incrementOutcomeData(strOutcome)
        }
        if (boolCrimeExists === false) {
          app.arrCrimeChartData.push({
            'category': strCrime,
            'count': 1
          });
        } else {
          app.incrementCrimeChartData(strCrime);
        }
      });
    },
    drawCrimeChart: function(array) {
      var chartData = [];
      chartData.push(['Crime', 'Total']);
      for (var i = 0; i < array.length; i++) {
        var name = array[i].category.replace(/-/g, ' ').toProperCase();
        var total = array[i].count;
        chartData.push([name, total]);
      }
      var data = google.visualization.arrayToDataTable(chartData);
      var chart = new google.visualization.PieChart(document.getElementById('crime-chart'));
      chart.draw(data, app.pieoptions);
    },
    drawOutcomeCrimeChart: function(array) {
      var chartData = [];
      chartData.push(['Crime', 'Total']);
      for (var i = 0; i < array.length; i++) {
        var name = array[i].outcome.replace(/-/g, ' ').toProperCase();
        if (array[i].outcome === 'N/A') {
          name = "Not Available"
        }
        if (array[i].outcome === '') {
          name = "Not Available"
        }
        if (array[i].outcome === null) {
          name = "Not Available"
        }
        var total = array[i].count;
        chartData.push([name, total]);
      }
      var data = google.visualization.arrayToDataTable(chartData);
      var chart = new google.visualization.PieChart(document.getElementById('outcome-chart'));
      chart.draw(data, app.pieoptions);
    },
    processCrimeMarkers: function() {
      var crimescount = app.crimes.length;
      app.addMarker('user', app.position[0], app.position[1], 'You are at latitude: ' + app.position[0] + ', longitude' + app.position[1]);
      for (var i = 0; i < crimescount; i++) {
        var crm = app.crimes[i];
        var markertype = 'Crime'
        var markerlocation = new google.maps.LatLng(crm.item.data.location.latitude, crm.item.data.location.longitude);
        var icon = '/img/icons/white/crimescene.png';
        var marker = new google.maps.Marker({
          position: markerlocation,
          map: map,
          markerid: crm.item.data.id,
          mk_markertype: markertype,
          title: crm.item.data.category.replace(/-/g, ' ').toProperCase(),
          icon: icon
        });
        app.markers.push(marker);
      }
      app.autoCenter();
    },
    processSchoolMarkers: function() {
      var schoolcount = app.schools.length;
      app.addMarker('user', app.position[0], app.position[1], 'You are at latitude: ' + app.position[0] + ', longitude' + app.position[1]);
      for (var i = 0; i < schoolcount; i++) {
        var sch = app.schools[i].school;
        var markertype = 'School'
        var markerlocation = new google.maps.LatLng(sch.Latitude, sch.Longitude);
        var icon;
        if (sch.Phase_of_education == 'Nursery') {
          icon = '/img/icons/schools/Nursery.png';
        }
        if ((sch.Phase_of_education == 'Primary') || (sch.Phase_of_education == 'Middle Deemed Primary')) {
          icon = '/img/icons/schools/Primary.png';
        }
        if ((sch.Phase_of_education == 'Secondary') || (sch.Phase_of_education == 'Middle Deemed Secondary')) {
          icon = '/img/icons/schools/Secondary.png';
        }
        if (sch.Phase_of_education == 'Not applicable') {
          icon = '/img/icons/schools/Notapplicable.png';
        }
        if (sch.Phase_of_education == 'All Through') {
          icon = '/img/icons/schools/AllThrough.png';
        }
        if (sch.Phase_of_education == '16 Plus') {
          icon = '/img/icons/schools/16Plus.png';
        }
        var marker = new google.maps.Marker({
          position: markerlocation,
          map: map,
          markerid: sch.ID,
          mk_markertype: markertype,
          title: sch.Establishment_name,
          icon: icon
        });
        app.markers.push(marker);
      }
      app.autoCenter()
    },
    processCarMarkers: function(){
      var carsCount = app.cars.length;
      var cars = app.cars;
      app.addMarker('user', app.position[0], app.position[1], 'You are at latitude: ' + app.position[0] + ', longitude' + app.position[1]);
      for (var i = 0; i < carsCount; i++) {
        app.addMarker('car', app.cars[i].latitude, app.cars[i].longitude, app.cars[i].title, app.cars[i].id);
      }
    },
    processCourtMarkers: function() {
      var countsCount = app.courts.length;
      var courts = app.courts;
      app.addMarker('user', app.position[0], app.position[1], 'You are at latitude: ' + app.position[0] + ', longitude' + app.position[1]);
      for (var i = 0; i < countsCount; i++) {
        app.addMarker('court', app.courts[i].item.data.lat, app.courts[i].item.data.lon, app.courts[i].item.data.name, 'court_' + app.courts[i].item.data.number);
      }
    },
    processPropertyMarkers: function(arrname){
      var propertiescount = app[arrname].listing.length;
      //app.clearMarkers();
      app.addMarker('user', app.position[0], app.position[1], 'You are at latitude: ' + app.position[0] + ', longitude' + app.position[1]);

      var properties = app[arrname].listing;
      for (var i = 0; i < propertiescount; i++) {
        //(markertype, lat, lng, title, id)
        app.addMarker( 'property',app[arrname].listing[i].latitude,app[arrname].listing[i].longitude, app[arrname].listing[i].displayable_address, 'property_' +app[arrname].listing[i].listing_id)
      }

    },
    getData: function(collection) {

      app.currenterror = null;
      if (collection === 'broadband') {
        app.currentWindow = null;
        // TODO - ONLY DO THIS ONCE PER POSTCODE!
        axios.get('/broadband/' + app.postcodes.item.data[0].postcode.replace(' ', ''))

          .then(function(response) {
            app.showbroadband = true;
            app.currentWindow = 'broadband';
            app.broadband = response.data[0];
            app.setAttributeToNew('map', 'class', 'col-sm-8');
            app.autoCenter();
          })
          .catch(function(error) {
            app.currentWindow = 'broadbanderror';
            app.currenterror = {
              title: 'Sorry, something has gone wrong',
              message: 'Oops, it seems there is a problem with the server-side Broadband stats functionality, and loading data has failed. The error response is below... ',
              error: error.response,
              action: 'broadband'
            }

          });
      } else if (collection === 'cars') {

        var carsearchdata = JSON.parse(app.carsearch)
        carsearchdata.page = app.propertypagination.cars.currentpage
        this.currentpropertysearchtype = 'cars'
        app.cars = []
        // TODO - ONLY DO THIS ONCE PER POSTCODE!
        axios.defaults.headers.common['csrfToken'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content')
        axios.post('/cars/search', carsearchdata )
          .then(function(response) {
            app.currentWindow = 'cars';
            app.showbroadband = false;
            app.carsloaded = true
            app.cars = response.data.results;
            app.loadingsearchdata = false;
            app.currentpropertysearchtype = 'cars',
            app.cartotalresults = response.data.count;
            app.propertypagination.cars.paginationcount = Math.ceil(app.cartotalresults / app.searchresultscount);
            app.carsloaded = true;
            app.processCarMarkers();
            app.calculateNewRecordStart();
            app.calculateNewRecordEnd();
            app.setAttributeToNew('map', 'class', 'col-sm-8');
            app.autoCenter()
          })
          .catch(function(error) {
            //app.currentWindow = 'broadbanderror';
            app.loadingsearchdata = false
            app.currenterror = {
              title: 'Sorry, something has gone wrong',
              action: 'cars',
              message: 'Oops, it seems there is a problem with the server-side Motors search functionality, and the search has failed. Please try again',
              error: error.response
            }
          });
      } else if (collection === 'schools') {
        app.currentWindow = null;
        // TODO - ONLY DO THIS ONCE PER POSTCODE!
        axios.get('/schools/' + app.postcodes.item.data[0].postcode)
          .then(function(response) {
            app.showbroadband = false;
            app.schools = response.data.schools;
            app.currentWindow = 'schools';
            app.processSchoolMarkers();
            app.setAttributeToNew('map', 'class', 'col-sm-8');
            app.autoCenter()
          })
          .catch(function(error) {
            app.loadingsearchdata = false;
            app.currentWindow = 'schools';
            app.currenterror = {
              title: 'Sorry, something has gone wrong',
              action: 'schools',
              message: 'Oops, it seems there is a problem with the server-side Schools finder functionality, and the search has failed. Please try again',
              error: error.response
            }
          });
      } else if (collection === 'crime') {
        app.currentWindow = null;
        axios.get('/crime/' + app.position[0] + '/' + app.position[1])
          .then(function(response) {
            app.showbroadband = false;
            app.currentWindow = 'crime';
            app.crimes = response.data.results;
            app.processCrimeDataforCharts();
            app.processCrimeMarkers();
            app.setAttributeToNew('map', 'class', 'col-sm-8');
            app.autoCenter();
          })
          .catch(function(error) {
            app.loadingsearchdata = false;
            app.currentWindow = 'crime';
            app.currenterror = {
              title: 'Sorry, something has gone wrong',
              action: 'crime',
              message: 'Oops, it seems there is a problem with the server-side Street Level Crime functionality, and the search has failed. Please try again',
              error: error.response
            }
          });
      } else if (collection === 'courts') {
        app.currentWindow = null;
        axios.get('/courts/' + app.postcodes.item.data[0].postcode.replace(/ /g, ''))
          .then(function(response) {
            app.showbroadband = false;
            app.currentWindow = 'courts';
            app.courts = response.data.results;
            app.currentWindow = 'courts';
            app.setAttributeToNew('map', 'class', 'col-sm-8');
            app.processCourtMarkers();
          })
          .catch(function(error) {
            app.loadingsearchdata = false;
            app.currentWindow = 'courts';
            app.currenterror = {
              title: 'Sorry, something has gone wrong',
              action: 'courts',
              message: 'Oops, it seems there is a problem with the server-side Court & Tribunal finder functionality, and the search has failed. Please try again',
              error: error.response
            }
          });
      }
    },
    putUserBack: function() {
      app.addMarker('user', app.position[0], app.position[1], 'You are at latitude: ' + app.position[0] + ', longitude' + app.position[1]);
    },
    getPropertSales: function(path) {
      app.loadingsearchdata = true;
      axios.get(path)
        .then(function(response) {
          app.showbroadband = false;
          app.properties = response.data;
          app.showpropertysalessearch = false;
          app.propertyloaded = true;
          app.loadingsearchdata = false;
          app.paginationcount = Math.ceil(app.properties.result_count / app.searchresultscount);
          app.paginationcountSales = Math.ceil(app.properties.result_count / app.searchresultscount);
          app.propertypagination.sales.paginationcount = Math.ceil(app.properties.result_count / app.searchresultscount);
          app.setAttributeToNew('map', 'class', 'col-sm-6');
          app.calculateNewRecordStart();
          app.calculateNewRecordEnd();
          app.processPropertyMarkers('properties');
          //app.autoCenter();
        })
        .catch(function(error) {

          app.loadingsearchdata = false;
          app.currentWindow = 'properties';
          app.currentpropertysearchtype = 'Sales';
          app.currenterror = {
            title: 'Sorry, something has gone wrong',
            action: 'sales',
            message: 'Oops, it seems there is a problem with the server-side Property Sales Search functionality, and the search has failed. Please try again',
            error: error.response
          }
        });
    },
    resetPropertySearchPanel: function(event) {
      if (app.currentpropertysearchtype === 'Sales') {
        app.showpropertysalessearch = true;
        app.properties = [];
        app.propertypagination.sales.currentstart = 1;
        app.propertypagination.sales.currentend = 20;
        app.propertypagination.sales.currentpage = 1;
        app.propertypagination.sales.paginationcount = 0;
      } else if (app.currentpropertysearchtype === 'Lettings') {
        app.showpropertylettingssearch = true;

        app.propertypagination.lettings.currentstart = 1;
        app.propertypagination.lettings.currentend = 20;
        app.propertypagination.lettings.currentpage = 1;
        app.propertypagination.lettings.paginationcount = 0;
        app.lettings = [];
      } else {
        app.putUserBack()
        //  app.showlettingssearch = true;
        app.showpropertylettingssearch = true;
        app.propertypagination.lettings.currentstart = 1;
        app.propertypagination.lettings.currentend = 20;
        app.propertypagination.lettings.currentpage = 1;
        app.propertypagination.lettings.paginationcount = 0;
        app.lettings = [];

        app.showpropertysalessearch = true;
        app.properties = [];
        app.propertypagination.sales.currentstart = 1;
        app.propertypagination.sales.currentend = 20;
        app.propertypagination.sales.currentpage = 1;
        app.propertypagination.sales.paginationcount = 0;
      }
      if (event) {
        event.preventDefault();
        return false;
      }
    },
    getPropertyLettings: function(path) {
      app.loadingsearchdata = true;
      axios.get(path)
        .then(function(response) {
          app.showbroadband = false;
          app.lettings = response.data;
          app.currentWindow = 'property';
          app.showpropertylettingssearch = false;
          app.propertyloaded = true;
          app.loadingsearchdata = false;
          app.paginationcount = Math.ceil(app.lettings.result_count / app.searchresultscount);
          app.pagination = Math.ceil(app.lettings.result_count / app.searchresultscount);
          app.propertypagination.lettings.paginationcount = Math.ceil(app.lettings.result_count / app.searchresultscount);
          app.setAttributeToNew('map', 'class', 'col-sm-6');
          app.calculateNewRecordStart();
          app.calculateNewRecordEnd();
          app.processPropertyMarkers('lettings');
          //app.autoCenter();
        })
        .catch(function(error) {
          app.loadingsearchdata = false;
          app.currentWindow = 'property';
          app.showpropertylettingssearch = true;
          app.currentpropertysearchtype = 'Lettings';
          app.currenterror = {
            title: 'Sorry, something has gone wrong',
            action: 'lettings',
            message: 'Oops, it seems there is a problem with the server-side Property Lettings Search functionality, and the search has failed. Please try again',
            error: error.response
          }
        })
    },
    doMap: function() {
      if ("geolocation" in navigator) {
        /*
        TODO:  This can be be done more elegantly...
        All good, we have geolocation
        */
      } else {
        /* geolocation IS NOT available */
        app.nolocation = true;
      }
      var lat = app.defaultpoint[0];
      var lng = app.defaultpoint[1];
      window.map = new google.maps.Map(document.getElementById('map'), {
        center: {
          lat: lat,
          lng: lng
        },
        zoom: app.currentZoom,
        disableDefaultUI: true,
        scrollwheel: false,
        navigationControl: false,
        mapTypeControl: false,
        scaleControl: false,
        draggable: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });
      google.maps.event.addDomListenerOnce(window.map, 'idle', function() {
         var rpoint = new google.maps.LatLng(lat, lng)
        google.maps.event.addDomListener(window, 'resize', function() {
          window.map.setCenter(rpoint);
        });
      });
    },
    setMapOnAll: function(map) {
      for (var i = 0; i < app.markers.length; i++) {
        app.markers[i].setMap(map);
      }
    },
    clearMarkers: function() {
      app.setMapOnAll(null);
    },
    showMarkers: function() {
      app.setMapOnAll(map);
    },
    deleteMarkers: function() {
      app.clearMarkers();
      app.markers = [];
    },
    selectMarker: function(id, zoom) {
      $.each(app.markers, function(index, marker) {
        if (marker.markerid == id) {
          map.setZoom(zoom)
          app.currentZoom = zoom;
          map.panTo(marker.position);
          marker.setAnimation(google.maps.Animation.BOUNCE);


          setTimeout(function() {
            marker.setAnimation(null);
          }, 5000);


        } else {
          marker.setAnimation(null);
        }
      });
    },
    truncateString: function(strToTrim, newLength) {
      var cut = strToTrim.indexOf(' ', newLength);
      if (cut == -1) return strToTrim;
      return strToTrim.substring(0, cut) + '...'
    },
    setAttributeToNew: function(el, attrib, val) {
      document.getElementById(el).setAttribute(attrib, val);
      google.maps.event.trigger(window.map, 'resize')
    },
    autoCenter: function() {
      //  Create a new viewpoint bound
      var bounds = new google.maps.LatLngBounds();
      //  Go through each...
      $.each(app.markers, function(index, marker) {
        bounds.extend(marker.position);
      });
      //  Fit these bounds to the map
      map.fitBounds(bounds);
      if (app.markers.length === 1) {
        map.setZoom(app.currentZoom);
      }
    },
    addMarker: function(markertype, lat, lng, title, id) {
      console.log("adding " + markertype + " icon");
      if (markertype === 'user') {
        app.deleteMarkers();
      }
      var markerlocation = new google.maps.LatLng(lat, lng);
      var icon, markerid;
      if (markertype === 'user') {
        markerid = 'user',
          icon = '/img/icons/orange/location.png';
      } else if (markertype === 'court') {
        markerid = id,
        icon = '/img/icons/white/court.png';
      }
      else if (markertype === 'car') {
       markerid = id,
       icon = '/img/icons/white/car.png';
     } else if (markertype === 'property') {
        markerid = id,
        icon = '/img/icons/white/property.png';
      } else {
        markerid = 'default',
          icon = '/img/icons/white/blank.png';
      }
      var marker = new google.maps.Marker({
        position: markerlocation,
        map: map,
        title: title,
        markerid: markerid,
        mk_markertype: markertype,
        icon: icon
      });
      app.markers.push(marker);
      map.panTo(markerlocation);
      if (markertype === 'user') {
        map.setZoom(15);
        app.currentZoom = 15;
      }
        app.autoCenter();
    },
    getLatLngFromPostcode: function(postcode) {
      axios.get('/postcodes/convert/postcode/' + postcode)
        .then(function(response) {
          var latitude = response.data.result.latitude;
          var longitude = response.data.result.longitude;
          app.position = [latitude, longitude];
          app.addMarker('user', latitude, longitude, 'You are at latitude: ' + latitude + ', longitude' + longitude);
          app.postcodeerror = false;
          app.wronglocation = false;
          axios.get('/postcodes/convert/latlng/' + latitude + '/' + longitude)
            .then(function(response) {
              app.postcodes = response.data.results[1];
            })
            .catch(function(error) {
              console.error(error);
            });
        })
        .catch(function(error) {
          console.error(error);
        });
    },
    validatePostCode: function(postcode) {
      axios.get('/postcodes/validate/' + postcode)
        .then(function(response) {
          if (response.data.result === true) {
            app.getLatLngFromPostcode(postcode)
          } else {
            app.postcodeerror = true;
          }
        })
        .catch(function(error) {
          console.error(error);
        });
    },
    resetLocationFromPostcode: function(postcode) {
      app.validatePostCode(postcode);
    },
    findUser: function() {
      if (!navigator.geolocation) {
        console.error("Geolocation is not supported by your browser");
        return;
      }
      navigator.geolocation.getCurrentPosition(function(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        app.position = [latitude, longitude];
        app.addMarker('user', latitude, longitude, 'You are at latitude: ' + latitude + ', longitude' + longitude);
        axios.get('/postcodes/convert/latlng/' + latitude + '/' + longitude)
          .then(function(response) {
            app.postcodes = response.data.results[1];
          })
          .catch(function(error) {
            console.error(error);
          });
      }, function() {
        console.error("Unable to retrieve user location");
      });
    }
  }
});

app.doMap();

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    //
    protected $table = "vw_schools_with_lat_lngs";
    public $timestamps = false;
}

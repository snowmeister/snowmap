<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostcodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function getPostcodesFromLatLng($lat, $lng)
     {
       $url = "https://api.postcodes.io/postcodes?lon=$lng&lat=$lat&radius=1999";
       $postcodes = json_decode($this->get_data($url));
       $arrpostcodes = [];
       foreach ($postcodes as $postcode) {
         # code...
         $postcodedata = [
           "data"=>$postcode,
           "point_type"=>"postcode",
           "point"=>[$lat, $lng]
         ];
         $arrpostcodes[]=["item"=>$postcodedata];
       }
       $arrData = ["results"=>$arrpostcodes, "total"=>count($arrpostcodes)];
       return $arrData ;
     }
     public function getLatLngFromPostcode($postcode)
     {
       $url = "https://api.postcodes.io/postcodes/$postcode";
       $data = json_decode($this->get_data($url));
       return response()
            ->json($data);
     }
     public function validatePostCode($postcode)
     {
       $url = "https://api.postcodes.io/postcodes/$postcode/validate";
       $isValidPostcode = json_decode($this->get_data($url));
       return response()
            ->json($isValidPostcode);
     }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Log;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function get_data($url)
    {
            $ch = curl_init();
            $timeout = 5;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $data = curl_exec($ch);
            curl_close($ch);
            Log::info('CURL.....HERE WE ARE');
            Log::info($data);
            return $data;

  }
  public function generateOutcodeRange($prefix,$start,$range)
  {
    $before = $start-$range;
    $end = $start+$range;
    $prefix = preg_replace('/\d/', '', $prefix );
    return [$prefix[0].$before, $prefix[0].(int)$start, $prefix[0].$end];
  }

  public function getNearbyOutcodes($outcode)
  {
    $url = "https://api.postcodes.io/outcodes/$outcode/nearest";

    $outcodes = json_decode($this->get_data($url));
    $arrOutcodes = [];
    if($outcodes->result){
      foreach ($outcodes->result as $outcode) {
        $arrOutcodes[] = $outcode->outcode;
      }
    }
    return $arrOutcodes;
  }
  public function getNearbyPostcodesForPostcode($postcode)
  {
    $url = "https://api.postcodes.io/postcodes/$postcode/nearest?radius=1999";
    $postcodes = json_encode($this->get_data($url));
    $arrpostcodes = [];
    return json_decode($postcodes);
  }
  public function getAddressLocatoinData($address){
    $url ="https://maps.googleapis.com/maps/api/geocode/json?address=$address&key=AIzaSyC2OTxwUNP9EfI-lMuShbEAgRWt6CzsiYc";
    $addressData = json_encode($this->get_data($url));
    return $addressData;
  }
}

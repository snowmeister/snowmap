<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SchoolsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function listByTownAndType($type, $town)
    {
        if($type != 'all'){
          $schools = \App\School::where('Phase_of_education', '=', $type)->where('Town', '=', $town)->get();
        }else{
          $schools = \App\School::where('Town', '=', $town)->get();
        }
        $arrSchools = [];
        foreach ($schools as $school) {
          $strSearchPostcode = str_replace(' ', '', $school['Postcode']);
          $latlng = \App\Latlng::where('postcode', '=', $strSearchPostcode)->first();
          $schooldata = [
            "data"=>$school,
            "point_type"=>"school",
            "point"=>[$latlng['latitude'],$latlng['longitude']]
          ];
          $arrSchools[]=["item"=>$schooldata];
        }
        $arrData = ["results"=>$arrSchools, "total"=>count($arrSchools)];
       return $arrData ;
    }




    public function listByPostcode($postcode)
    {
        $count = 0;
        $postcode = explode(' ', $postcode);
        $outbasennum = preg_replace('~\D~', '', $postcode[0]);
        $range = $this->generateOutcodeRange($postcode,$outbasennum, 1);
        $arrschools = [];
        foreach ($range as $outcode) {
          # code...
          $schools = \App\School::where('Postcode', 'LIKE', "$outcode%" )->orderBy('Establishment_name', 'ASC')->get();
          foreach($schools as $school){
            // $postcode = \App\Postcode::where('Postcode', '=', $school->Postcode)->get();
            array_push($arrschools, ['school'=> $school]);
          }
        }
      
        $data = ["count"=>count($arrschools), "schools"=>$arrschools];
        return $data;

      //  return $range;
        //return $outbasennum;
        // $postcodes = json_decode($this->getNearbyPostcodesForPostcode($postcode));
        // $arrData = [];
        // $arrQuery = [];

        //dd($postcodes->result);
        // foreach ($postcodes->result as $postcode) {
        //   # code...
        //   $arrData[] = ['postcode' => $postcode->postcode, 'latitude'=> $postcode->latitude, 'longitude'=> $postcode->longitude];
        //   $arrQuery[] = $postcode->parliamentary_constituency;
        // }
        // $schools = \App\School::whereIn('Town', $arrQuery)
        //             ->get();
        // $arrResponse = ['schools'=>$schools];
        // return $arrResponse;

    }
}

<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
      <meta name="csrf-token" content="{{ csrf_token() }}">
        <script>window.Laravel = { csrfToken: '{{ csrf_token() }}' }</script>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>SnowMap</title>
        <!-- Fonts -->
        <link href="//fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"  rel="stylesheet" type="text/css">
        <!-- Styles -->
        <link href="css/app.css" rel="stylesheet" type="text/css">
    </head>
    <body>
      <div id="app">
        <nolocation transition="fade" transition-mode="out-in" v-if="nolocation === true"></nolocation>
        <locationdisplay transition="fade" transition-mode="out-in" v-if="currentWindow === 'userlocation'"></locationdisplay>
        <broadband  transition="fade" transition-mode="out-in" v-if="currentWindow === 'broadband'"></broadband>
        <broadbanderror  transition="fade" transition-mode="out-in" v-if="currentWindow === 'broadbanderror'" v-model="$root.currenterror"></broadbanderror>
        <crime  transition="fade" transition-mode="out-in"v-if="currentWindow === 'crime'"></crime>
        <tools transition="fade" transition-mode="out-in" v-if="(position !== null) && (wronglocation === false)"></tools>
        <schools transition="fade" transition-mode="out-in" v-if="currentWindow === 'schools'"></schools>
        <property transition="fade" transition-mode="out-in" v-if="currentWindow === 'property'" v-bind="{
          'currentpropertysearchtype': $root.currentpropertysearchtype
        }"></property>
        <courts transition="fade" transition-mode="out-in" v-if="currentWindow === 'courts'"></courts>
        <maptype transition="fade" transition-mode="out-in" v-if="currentWindow === 'maptype'"></maptype>
        <cars transition="fade" transition-mode="out-in" v-if="currentWindow === 'cars'"></cars>
        <about transition="fade" transition-mode="out-in" v-if="currentWindow === 'about'"></about>

        <startbox transition="fade" transition-mode="out-in" v-show="position === null"></startbox>
          {{-- <datamenu v-show="position === null"></datamenu> --}}

</div>
        <div id="map" class="col-sm-12"></div>
    </body>
 <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyC2OTxwUNP9EfI-lMuShbEAgRWt6CzsiYc"></script>
 <script type="text/javascript" src="//www.google.com/jsapi"></script>
 <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.min.js"></script>

 </script>
 <script type="text/javascript">
 function drawChart(){
   // TODO - Not convinced this is the best way of doing this...
 }
  // Load the Visualization API library and the piechart library.
  google.load('visualization', '1.0', {'packages':['corechart']});
  // Shouldnt really need this, as I am NOT calling the charts onload! But things go BANG if this is omitted!
  google.setOnLoadCallback(drawChart);
</script>
 <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
 <script src="js/app.js"></script>
</html>

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
class CarsController extends Controller
{

    public function search(Request $request){
      $input = (array) $request->all();
      $adzKey =  env('ADZUNA_API_KEY');
      $adzAppId =  env('ADZUNA_APPID');
      $q = '&';
      foreach($input as $key => $value){
        if(($key != 'page') && ($key != '_token')){
          if($value !=''){
            $q = $q . $key .'='.urlencode($value) .'&';
          }
        }
        if($key == 'page'){
          $page = $value;
        }
      }
      Log::info('calling page: '.$page);
      $url = "https://api.adzuna.com/v1/api/cars/gb/search/$page?app_id=$adzAppId&app_key=$adzKey$q"."results_per_page=20";
      $cars = $this->get_data($url);
      return $cars;
    }


}

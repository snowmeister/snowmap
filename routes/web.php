<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/* Basic data routes - TODO - These need better orgnaisation! API? */
Route::get('/schools/{postcode}', 'SchoolsController@listByPostcode');
Route::get('/broadband/{postcode}', 'BroadbandController@getBroadBandStatsByPostcode');
Route::get('/courts/{postcode}', 'CourtsController@getCourtsByPostcode');
Route::get('/crime/{lat}/{lng}', 'CrimeController@getCrimeByLatLng');
Route::get('/pricepaid/{postcode}', 'PricepaidController@getPropertiesByPostcode');
Route::get('/postcodes/convert/latlng/{lat}/{lng}', 'PostcodeController@getPostcodesFromLatLng');
Route::get('/postcodes/validate/{postcode}', 'PostcodeController@validatePostCode');
Route::get('/postcodes/convert/postcode/{postcode}', 'PostcodeController@getLatLngFromPostcode');
Route::get('/property/sales/{lat}/{lng}/{radius}/{minprice}/{maxprice}/{numbedrooms}/{page}/{count}/', 'PropertyController@getZoooplaSalesSearch');
Route::get('/property/lettings/{lat}/{lng}/{radius}/{minprice}/{maxprice}/{numbedrooms}/{page}/{count}/', 'PropertyController@getZoooplaLettingSearch');
Route::get('/weather/{lat}/{lng}/', 'WeatherController@getForecasts');
Route::post('/cars/search/', 'CarsController@search');

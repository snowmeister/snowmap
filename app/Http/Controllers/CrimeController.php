<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CrimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }





    public function getCrimeByLatLng($lat, $lng) {
      $url = "https://data.police.uk/api/crimes-street/all-crime?lat=$lat&lng=$lng";
      log::info($url);
      $crimes = json_decode($this->get_data($url));
      log::info($crimes);
      $arrCrimes = [];
      //if($crimes != null){

      foreach ($crimes as $crime) {
        # code...
        $crimedata = [
          "data"=>$crime,
          "point_type"=>"crime",
          "point"=>[$crime->location->latitude,$crime->location->longitude]
        ];
        $arrCrimes[]=["item"=>$crimedata];
      }
      $arrData = ["results"=>$arrCrimes, "total"=>count($arrCrimes)];
      return $arrData ;
  //  }
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

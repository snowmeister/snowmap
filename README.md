# Snowmap v2 #

This is the second iteration of my personal mashup project [Snowmap](https://map.snowmeister.co.uk), which uses various web APIs to give users quick and simple tools to find useful information about the area surrounding their location (or postcode if they decide not to allow HTML5 geolocation)

Built as a way of exploring ideas and techniques I have not had chance to use in my full-time role, or that I am keen to learn or evaluate.

At this point, this web app is DESKTOP ONLY - I intend to create a mobile version soon, but its my feeling that full screen map apps don't work as well on small devices, so the mobile version will have a completely different UI, rather than being responsive.

## Stack ##

This app is built on [Laravel 5.4](https://laravel.com/) on the LAMP stack for the backend and uses [VueJS](https://vuejs.org/) v2 and [Bootstrap](http://getbootstrap.com) v3 on the front-end, and is currently running on a [DigitalOcean](https://m.do.co/c/784f4821b115) droplet running Ubuntu 14.04 LTS server.

## APIs & Data ##

1. [Google Maps](https://developers.google.com/maps/documentation/javascript/) v3 API
1. [Google Charts](https://developers.google.com/chart/) API
1. [Adzuna API](https://developer.adzuna.com/) for Motors Search
1. [Zoopla API](https://developer.zoopla.co.uk/) for Property Sales & Lettings Search
1. [UK Police API](https://data.police.uk/docs/) for Street Level Crime data
1. [Postcodes.io API](https://postcodes.io/) for Lat/Lng to Postcode Geocoding
1. [Courtfinder's API](https://courttribunalfinder.service.gov.uk/api.html) to look up Courts and Tribunals.
1. Various datasets from the [Data.gov.uk](https://data.gov.uk/data/search) open datasets, including UK Schools, and Broadband speeds
